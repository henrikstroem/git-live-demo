# file: flyers.py

import sys


def fly_sound(flyer: str):
    """Print the sound of flying objects.

    >>> fly_sound("airplane")
    airplane: wroom
    >>> fly_sound("duck")
    duck: flap flap
    >>> fly_sound("helicopter")
    helicopter: chop chop
    """
    sound = {
            'airplane': 'wroom',
            'duck': 'flap flap',
            'helicopter': 'chop chop'
            }
    print(f"{flyer}: {sound[flyer]}")


def main():
    try:
        flyer = sys.argv[1]
    except IndexError:
        flyer = "duck"
    fly_sound(flyer)


if __name__ == '__main__':
    import doctest
    doctest.testmod()

    main()
